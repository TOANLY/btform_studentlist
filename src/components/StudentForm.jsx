import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { studentActions } from "../store/student/slice";

const StudentForm = () => {
  const { studentList, studentInfo } = useSelector(
    (state) => state.studentReducer
  );
  const [formData, setFormData] = useState({
    code: "",
    phone: "",
    fullName: "",
    email: "",
  });
  const [formError, setFormError] = useState({
    code: "",
    phone: "",
    fullName: "",
    email: "",
  });
  const ValidateAll = (value, name, validity) => {
    let mess;
    if (!value) {
      return (mess = "Vui lòng nhập thông tin");
    } else if (validity.patternMismatch && name === "code") {
      return (mess = "vui lòng nhập số");
    } else if (name === "code" && !studentInfo) {
      const duplicateIndex = studentList.findIndex(
        (student) => student.code === value
      );
      if (duplicateIndex !== -1) {
        return (mess = "Mã sinh viên đã tồn tại");
      }
    } else if (
      validity.patternMismatch &&
      (name === "phone" || name === "email")
    ) {
      return (mess = "Sai định dạng");
    }
  };
  const dispatch = useDispatch();
  const handleFormData = () => (e) => {
    const { name, value, validity } = e.target;
    const mess = ValidateAll(value, name, validity);
    setFormError((formError) => ({
      ...formError,
      [name]: mess,
    }));
    setFormData((formData) => ({
      ...formData,
      [name]: mess ? undefined : value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const formElement = document.getElementById("form");
    const inputElements = formElement.querySelectorAll("input");
    let newFormError = { ...formError };
    inputElements.forEach((ele) => {
      const { name, value, validity } = ele;
      const mess = ValidateAll(value, name, validity);
      newFormError[name] = mess;
    });
    setFormError(newFormError);
    let flag = false;
    for (const key in newFormError) {
      if (newFormError[key]) {
        flag = true;
        break;
      }
    }
    if (flag) {
      return;
    }
    if (!studentInfo) {
      dispatch(studentActions.addStudent(formData));
      setFormData({ code: "", phone: "", fullName: "", email: "" });
    } else {
      dispatch(studentActions.updateStudent(formData));
      setFormData({ code: "", phone: "", fullName: "", email: "" });
    }
  };
  useEffect(() => {
    if (!studentInfo) return;
    setFormData(studentInfo);
  }, [studentInfo]);

  return (
    <form id="form" noValidate onSubmit={handleSubmit} className="text-left">
      <div className="mt-4">
        <div className="row">
          <div className="form-group col-6 ">
            <label className="font-weight-bold " htmlFor="exampleInputEmail1">
              Mã SV
            </label>
            <input
              disabled={studentInfo ? true : false}
              pattern="^[0-9]+$"
              minLength={1}
              name="code"
              value={formData?.code}
              onChange={handleFormData()}
              className="form-control"
              aria-describedby="emailHelp"
            />
            <span className="text-danger">{formError.code}</span>
          </div>{" "}
          <div className="form-group col-6">
            <label className="font-weight-bold" htmlFor="exampleInputEmail1">
              Họ tên
            </label>
            <input
              minLength={1}
              value={formData?.fullName}
              name="fullName"
              onChange={handleFormData()}
              className="form-control"
              aria-describedby="emailHelp"
            />
            <span className="text-danger">{formError.fullName}</span>
          </div>
          <div className="form-group col-6">
            <label className="font-weight-bold" htmlFor="exampleInputPassword1">
              Số điện thoại
            </label>
            <input
              pattern="^(84|0[35789])(\d{8})$"
              minLength={1}
              value={formData?.phone}
              name="phone"
              onChange={handleFormData()}
              type="text"
              className="form-control"
            />
            <span className="text-danger">{formError.phone}</span>
          </div>
          <div className="form-group col-6">
            <label className="font-weight-bold" htmlFor="exampleInputPassword1">
              Email
            </label>
            <input
              minLength={1}
              value={formData?.email}
              onChange={handleFormData()}
              type="email"
              className="form-control"
              name="email"
              pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
            />
            <span className="text-danger">{formError.email}</span>
          </div>
        </div>
      </div>

      <button
        style={studentInfo ? { display: "none" } : { display: "inline-block" }}
        className="btn btn-warning"
      >
        Thêm sinh viên
      </button>
      
    </form>
  );
};

export default StudentForm;
