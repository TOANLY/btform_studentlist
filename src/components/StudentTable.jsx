import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { studentActions } from "../store/student/slice";

const StudentTable = ({ studentListResult }) => {
    // delete
    const dispatch = useDispatch();
    const handleDelete = (code) => {
        dispatch(studentActions.deleteStudent(code));
    };
    const handleEdit = (student) => {
        dispatch(studentActions.getStudentInfo(student));
    };
    return (
        <table className="mt-3 table">
            <thead className="font-weight-bold bg-dark text-white">
                <tr>
                    <td>Mã SV</td>
                    <td>Họ tên</td>
                    <td>Số điện thoại</td>
                    <td>Email</td>
                    <td>Tùy chỉnh</td>
                </tr>
            </thead>
            <tbody>
                {studentListResult?.map((student) => {
                    return (
                        <tr key={student.code}>
                            <td>{student.code}</td>
                            <td>{student.fullName}</td>
                            <td>{student.phone}</td>
                            <td>{student.email}</td>
                            <td>
                                <button
                                    type="button"
                                    className="btn btn-success"
                                    onClick={() => handleEdit(student)}
                                >
                                    Sửa
                                </button>
                                <button
                                    onClick={() => handleDelete(student.code)}
                                    type="button"
                                    className="btn btn-danger ml-1"
                                >
                                    Xóa
                                </button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
};

export default StudentTable;
