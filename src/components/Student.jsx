import React, { useState, useMemo } from "react";
import StudentForm from "./StudentForm";
import StudentTable from "./StudentTable";
import { useSelector } from "react-redux";

const Student = () => {
  const { studentList } = useSelector((state) => state.studentReducer);
  const [search, setSearch] = useState("");

  const handleSearchForm = (e) => {
    setSearch(e.target.value);
  };

  const studentListResult = useMemo(() => {
    return studentList.filter((student) => student.fullName.includes(search));
  }, [search, studentList]);

  return (
    <div className="container mt-5">
      <h1 className="bg-dark p-2 text-left text-light">Thông tin sinh viên</h1>

      <StudentForm />
      <hr />
      <div className="input-group mt-3 ">
        <input
          onChange={handleSearchForm}
          name="search"
          type="search"
          className="form-control rounded"
          placeholder="Tìm kiếm"
          aria-label="Search"
          aria-describedby="search-addon"
        />
      </div>
      <StudentTable studentListResult={studentListResult} />
    </div>
  );
};

export default Student;
