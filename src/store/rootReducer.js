import { combineReducers } from "redux";
import { studentReducer } from "./student/slice";

export const rootReducer = combineReducers({
    studentReducer,
});
