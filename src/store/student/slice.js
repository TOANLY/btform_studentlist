import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    studentList: [],
    studentInfo: undefined,
};

export const studentSlice = createSlice({
    name: "STUDENT",
    initialState,
    reducers: {
        addStudent: (state, { payload }) => {
            state.studentList.push(payload);
        },

        deleteStudent: (state, { payload }) => {
            console.log(payload);
            state.studentList = state.studentList.filter(
                (todo) => todo.code !== payload
            );
        },
        getStudentInfo: (state, { payload }) => {
            state.studentInfo = payload;
        },
        updateStudent: (state, { payload }) => {
            state.studentList = state.studentList.map((student) => {
                console.log(payload);
                if (student.code === payload.code) {
                    return payload;
                }
                return student;
            });
            state.studentInfo = undefined;
        },
    },
});

export const { actions: studentActions, reducer: studentReducer } =
    studentSlice;
